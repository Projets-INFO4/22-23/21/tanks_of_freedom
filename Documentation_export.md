# Portage from Godot 2 to Godot 3

## Before exporting

File change format  : 
- xscn to tscn
- xml to tres (dependency problems for gd files)

> Export from Godot 2.7 to 3.1 with Godot's IDE 2

## After exporting

List of changes from Godot 2 to Godot 3 :
*a -> b : a in Godot 2 become b in Godot 3*


- In tscn files :
    - CanvasItemShaderGraph -> CanvasItemMaterials

- In gd files :
    - is_visible() (class Spatial and CanvasItem) -> is_visible_in_tree() (class Spatial and CanvasItem)
    - is_hidden() (class Spatial and CanvasItem) -> !is_visible_in_tree() (class Spatial and CanvasItem)
    - Globals -> ProjectSettings
    - Paths to find ProjectSettings changed. Furthermore, PS.get() -> PS.get_setting().\
    Example for window's height: `ProjectSettings.get('display/height')` -> `ProjectSettings.get_setting("display/window/size/height")`\
    Use documentation to find the matching parameters: https://docs.godotengine.org/en/3.6/classes/class_projectsettings.html
    - AudioStreamPlayer : set_volume() -> set_volume_db()
    - Class RegEx completely changes between Godot 2 and Godot 3. In Godot 2, there is just one class RegEx. In Godot 3, there is always the class RegEx, but furthermore, there is another class RegExMatch that contains the result and the details of the request RegEx.search(). Example of changes in Issue #33.
    - In Godot 2 event.position gives relative position and in Godot 3 it gives the absolute position (event.position -> event.relative) (Issue #42)

    - If a method is not declared, associated classes must be extended
    - If a child and its parent have a method with the same name, Godot chose the parent's method. To use the child's one, change the name of the method.
    
## Work with issues 

- **#1: SamplePlayer discontinue in Godot 3:**\
Delete the node 'SamplePlayer' in game.tscn\
sound_controller.gd extends Control\
Delete the function load_samples() in sound_controller.gd\
Samples become a hashmap\
play() changes:\
    -> create a stream player\
    -> add to the parent (sound_controller.gd)\
    -> choose the correct audio file and play it\
    -> When the sound end, remove the player from the parent
       and the garbage collector recycles it

- **#4 : bag_aware and var interaction :**\
Some of the vars in files extending bag_aware.gd need to be local. Bag_aware is a dispatcher of vars but some of them need to be local. In that case, the execution went wrong and show the message "Invalid set index...". Verify if the var in question is in a file that extends bag_aware.gd and if its call is self.###. You will need to delete the self. and declare the var at the top of the file\
4 occurrences

- **#5 : AudioStreamPlayer discontinue the set_loop function :**\
You will need to get the AudioStream of this object instead and call the function set_loop(x) where x is the mode of the loop (https://docs.godotengine.org/en/stable/classes/class_audiostreamwav.html#class-audiostreamwav in the loop_mode section)\
stream_player.set_loop(true) -> stream_player.get_stream().set_loop(true)

- **#6 : is_hidden -> !(is_visible_in_tree)**\
5 occurrences

- **#7 : theme conflict**\
Theme is a var for the control node. It's conflicted w/ the theme var of the control node, that the map_controller extends.\
Rename in season.\
3 occurrences

- **#8: Boolean expression in the game_logic.gd :**
Calcul in integer but variables are boolean -> Rework condition into boolean expression

- **#9 and #31: Menu map doesn't appear and the camera doesn't move**
When you launch the game, you have normally a default map behind the GUI. Here it doesn't appear or it doesn't load.
    - the pixel scale was under the viewport, which make it invisible. Now it's under the main node "game" 
    - We need to change all research of this node to "/root/game/pixel_scale"\
Now for the viewport, you need a viewport container for the 2d nodes to be rendering\
So we need a new node of this type under the game node and have the viewport
node under it. 

- **#10: Comparaison of a dictionnary with bool**
New comparaison of item of the dictionnary with 1

- **#11: GUI Button small compared to the original game**

- **#12: Quit Button error doesn't work**

- **#13: Intro theme replay but shouldn't**

- **#14: Menu appears in the intro cutscene but shouldn't**

- **#16: Same that #14 but with the demo in the intro**

- **#17: Every intro object is visible at the same but shouldn't**

- **#18: Demo autorun after "click to start" interaction**

- **#19 and #20: Demo start twice at 3s interval and restart every 12s**

- **#21: MouseEvent wrong index, can't reach position x or y + #25: Button map editor in the main menu makes the game crash**
7 occurrences for x and y (14)

- **#22: Button AI speed in settings crash the game + #24: Button Camera speed crashes when pressed in main menu settings**
Same resolution (2 occurrences)

- **#26: In map editor, button undo build makes the game crash**

- **#27: In map editor, pressing preview button makes the game crash**

- **#28: Back button on Map editor**
is_visible() (class Spatial and CanvasItem) -> is_visible_in_tree()\
16 occurrences to resolve the issue + 5 -> 21 occurrences

- **#29: In Settings, GFX, Full-screen button "on" to "off" makes crash the game**
get_rect().size -> get_size() in Godot 3

- **#32: Camera doesn't move and doesn't initiate properly**

- **#33: In MapEditor, in preview, Play button doesn't work, and game crashes**

- **#34: Dialog boxes shifted to the right**
The margin now is relative to the X-axis. So for the right margin, if you
want to shift it in the viewport you need to get the opposite margin.
(look at hud_controller.gd, line 404 to 405)

- **#35: Claim function from building is not called properly in the first campaign mission**
Claim function totally rework for the storyteller\
Instead of relying on the abstract map, we look into the bag for an array
of building and we look for the correct building

- **#37: Viewport doesn't update size**

- **#38: In the main menu, the online button crashes**
connect -> m_connect (33 occurrences)

- **#39: During the demo, pressing escape to make the game crash**
Still a bug that already existed in Godot 2 version

- **#40: Statue textures are missing on maps**

- **#41: Can't play a sound from a unit**

- **#42: Camera goes away when you grab it**
Absolute coordinates instead of relative coordinates.

- **#43: Action map glitch and #44: Action of Storyteller and #49: When playing, some units are not selectable, and #46: object is not player in move_hand.gd**

- **#45: Button Skirmish doesn't work**

- **#48: String method to_json() doesn't work**
Method change : save_data.to_json() -> to_json(save_data)

- **#51: Load save func crash**


## Errors and notes about the game code

- Class EmptyControl of Godot 2 doesn't exist in Godot 3 -> maybe replaced by Node but can't verify => used by unused files (storyteller/tutorial.gd and storyteller/tutorial_message.gd)
- Error of indentation: 4 spaces instead of Tabulation
- is_visible doesn't exist in Godot 3 but a method has been created in scripts/message.gd
- Bad name : get_instance_ID() -> get_instance_id()

## Warning!

- Each class not integer inherits from the object class (for Godot 2 and Godot 3)



