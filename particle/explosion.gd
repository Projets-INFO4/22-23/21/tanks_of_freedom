extends Node
var unit

func _die(anim_name):
	unit.clear_explosion()

func clear_points():
	unit.clear_floating_damage()

func show_ap_icon():
	get_node('AP').show()
