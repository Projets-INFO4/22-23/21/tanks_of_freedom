extends Control
export var trans_key = 'LABEL_DEFAULT'

func _ready():
	add_to_group("translate_me", false)
	refresh_label()
	pass

func refresh_label():
	var self_ref = self
	if self_ref is Label:
		self_ref.set_text(tr(self.trans_key))
	elif self_ref is Button:
		self_ref.set_text(tr(self.trans_key))
	else:
		pass

func set_trans_key(new_key):
	self.trans_key = new_key
	refresh_label()

