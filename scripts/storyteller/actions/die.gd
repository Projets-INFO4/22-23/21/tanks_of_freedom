extends "res://scripts/storyteller/actions/abstract_action.gd"

func perform(action_details):
	var unit_pos = action_details['who']
	
	for unit in self.bag.positions.units:
		if unit.position_on_map == unit_pos:
			unit.die_after_explosion(null)
			self.bag.root.sound_controller.play_unit_sound(unit, 'die')
			break

	self.bag.root.action_controller.collateral_damage(action_details['who'])

	self.bag.positions.refresh_units()

