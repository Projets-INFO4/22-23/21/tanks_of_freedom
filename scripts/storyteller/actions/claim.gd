extends "res://scripts/storyteller/actions/abstract_action.gd"

func perform(action_details):
	var claim_pos = action_details['what']
	
	for building in self.bag.positions.buildings:
		if building.position_on_map == claim_pos:
			building.claim(action_details['side'], 0)
			self.bag.root.sound_controller.play('occupy_building')
			self.bag.positions.refresh_buildings()
			return
	
