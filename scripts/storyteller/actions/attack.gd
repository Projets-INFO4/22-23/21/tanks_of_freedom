extends "res://scripts/storyteller/actions/abstract_action.gd"

func perform(action_details):
	var att_pos = action_details['who']
	var def_pos = action_details['whom']
	var att
	var def
	
	for unit in self.bag.positions.units:
		if unit.position_on_map == att_pos:
			att = unit
		if unit.position_on_map == def_pos:
			def = unit
		if att != null && def != null:
			def.show_explosion()
			self.bag.root.sound_controller.play_unit_sound(att, 'attack')
			self.bag.root.sound_controller.play_unit_sound(def, 'damage')

